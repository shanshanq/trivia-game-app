export function getCategories() {
  return fetch("https://opentdb.com/api_category.php").then((response) => {
    if (response.status !== 200) {
      throw new Error("Could not fetch the categoties!");
    }
    return response.json();
  });
}

export function getQuestions(amount, category, difficulty) {
  let url = `https://opentdb.com/api.php?amount=${amount}`;

  if (category) {
    url += `&category=${category}`;
  }
  if (difficulty) {
    url += `&difficulty=${difficulty}`;
  }

  return fetch(url)
    .then((response) => response.json())
    .then((response) => response.results)
    .then((response) => {
      return response.map((question) => {
        // Got a new answer array with all answers
        question.answers = [
          question.correct_answer,
          ...question.incorrect_answers,
        ];
        // Shuffle the answer array
        question.answers = shuffle(question.answers);
        return question;
      });
    });
}

// Shuffle the answers array
function shuffle(array) {
  var currentIndex = array.length,
    randomIndex;

  // While there remain elements to shuffle...
  while (currentIndex != 0) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;

    // And swap it with the current element.
    [array[currentIndex], array[randomIndex]] = [
      array[randomIndex],
      array[currentIndex],
    ];
  }

  return array;
}
