import Vue from "vue";
import VueRouter from "vue-router";
import StartScreen from "./components/StartScreen.vue";
import QuestionScreen from "./components/QuestionScreen.vue";
import ResultScreen from "./components/ResultScreen";

Vue.use(VueRouter); // Add the Router features to the Vue Object

const routes = [
  {
    path: "/start",
    alias: "/",
    component: StartScreen,
  },
  {
    path: "/questions",
    component: QuestionScreen,
  },
  {
    path: "/questions/:id",
    component: QuestionScreen,
  },
  {
    path: "/result",
    component: ResultScreen,
  },
];

export default new VueRouter({ routes });
